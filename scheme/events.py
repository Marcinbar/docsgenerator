from generator.helpers import path_from_text
from generator.scheme import BaseScheme
from generator.enums import Side
from generator.docs import EventDocs


class EventScheme(BaseScheme):
    template = 'templates/event.md'

    @staticmethod
    def get_path(docs: EventDocs) -> str:
        root_path = f'{docs.side.value}'
        if docs.side != Side.SHARED:
            root_path += '-side-events'
        else:
            root_path += '-events'

        sub_path = path_from_text(docs.category) if docs.category else "general"
        return f'{root_path}/{sub_path}/{docs.name}.md'




