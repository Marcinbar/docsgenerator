from generator.parser import SquirrelDoc
from generator.registry import TagRegistry
from generator.tags import NameTag, VersionTag, DeprecatedTag, SideTag, ParamTag, ReturnTag
from generator.validators import TagRequiredValidator, TagDuplicateValidator
from generator.docs import FunctionDocs
from scheme.function import FunctionScheme

docs = """
*   Super duper description of something.
*   AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA.
*
*   TEST DALEJ
*   @name       SuperFunction
*   @side       client
*   @deprecated Science dupa
*   @param      (int) a this is a super super duper shit xD
*   @param      (int) b
*   
*   @return    (int) Values
"""

scheme = FunctionScheme()

validators = [
    TagRequiredValidator(scheme.registry),
    TagDuplicateValidator(scheme.registry)
]

squirrel_doc = SquirrelDoc.parse(scheme.registry, docs)
for validator in validators:
    validator.validate(squirrel_doc.tags)


print(scheme.render(squirrel_doc.description, squirrel_doc.tags))
