import re
import os.path

from typing import Sequence, Optional
from os import walk

from .models import Param

_DEFAULT_EXTENSIONS = ['.cpp', '.h', '.hpp', '.nut']


def _is_directory_excluded(dir_path: str, exclude_paths: Sequence[str]) -> bool:
    dir_path = os.path.abspath(dir_path)
    for path in exclude_paths:
        if dir_path.startswith(path):
            return True

    return False


def collect_source_files(
    include_paths: Sequence[str],
    *,
    exclude_paths: Optional[Sequence[str]] = None,
    extensions: Optional[Sequence[str]] = None
):
    exclude_paths = [os.path.abspath(path) for path in exclude_paths or []]
    if extensions is None:
        extensions = _DEFAULT_EXTENSIONS

    result = []
    for path in include_paths:
        for (dir_path, dir_names, filenames) in walk(path):
            if _is_directory_excluded(dir_path, exclude_paths):
                continue

            for name in filenames:
                if any(name.endswith(ext) for ext in extensions):
                    result.append(f'{dir_path}/{name}')

    return result


def find_squirrel_docs(content: str):
    pattern = re.compile(r"\/(?:\*)+\s+squirreldoc\s+\((\w+)\)\s*\n(.*?)\*\/", re.MULTILINE | re.DOTALL)
    return pattern.findall(content)


def path_from_text(text: str) -> str:
    return '-'.join(text.lower().split(' '))


def format_param(param: Param) -> str:
    declaration = param.type + " " + param.name
    if param.default is not None:
        declaration += " = " + param.default

    return declaration
