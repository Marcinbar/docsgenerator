from typing import Type, Dict
from collections import OrderedDict

from .tags import AbstractTag


class TagRegistry(object):
    _registered: Dict[str, Type[AbstractTag]]

    def __init__(self):
        self._registered = OrderedDict()

    def register(self, tag: Type[AbstractTag]):
        self._registered[tag.identifier] = tag

    def values(self):
        return self._registered.values()

    def create(self, identifier: str, text: str):
        implementation = self._registered.get(identifier)
        if implementation is None:
            raise ValueError(f'Invalid tag used: {identifier}')

        return implementation.parse(text)
