import os
from typing import Mapping, Any
from abc import ABC
from jinja2 import Template


class BaseScheme(ABC):
    template: str

    def __init__(self, root_directory: str):
        self._template = Template(self._load(os.path.join(root_directory, self.template)), trim_blocks=True)

    @staticmethod
    def _load(path: str):
        with open(path, "r") as f:
            return f.read()

    def render(self, data: Mapping[str, Any]):
        return self._template.render(data)
